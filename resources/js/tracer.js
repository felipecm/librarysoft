/*
*
AUXILIAR METHODS
*
*/
var uniqueId = function(){
	var idstr=String.fromCharCode(Math.floor((Math.random()*25)+65));
	do {                
		var ascicode=Math.floor((Math.random()*42)+48);
		if (ascicode<58 || ascicode>64){
			idstr += String.fromCharCode(ascicode);    
		}                
	} while (idstr.length<8);
	idstr += new Date().getTime();
	return (idstr);
}

var getClientId = function(){
	if(! (typeof window != 'undefined' && window.document)){
    	return 'server';
    }
	if(!localStorage["clientIdDebugger"]) localStorage["clientIdDebugger"] = uniqueId();
	return localStorage["clientIdDebugger"];
}

/*
*
AUXILIAR METHODS FOR OBJECTS
*
*/
Array.prototype.next = function(){
	return this[0];
}
Array.prototype.getPending = function(){
	return this.slice(1,this.length);
}


function AssociativeArray(){
}

/*
*
Caseway MO
*
*/
function CAUSEWAY (debugMode){

	this.messagesList = new Array();
	this.nMessagesSent = -1;
	this.nMessagesGot = -1;
	this.sourceURL = ! (typeof window != 'undefined' && window.document) ? 'server.js' : document.url;
	
	this.DEBUG_MODE = debugMode;
	
	
	this.generateTrace = function (action,id,ev,loopCount){

		var number = -1;
		var event = 'undefined';
		if(ev == 'sent'){
			this.nMessagesSent = this.nMessagesSent+1;
			number = this.nMessagesSent;
			event = "org.ref_send.log.Sent";
		}
		else if(ev == 'got'){ 
			this.nMessagesGot = this.nMessagesGot+1;
			number = this.nMessagesGot;
			event = "org.ref_send.log.Got";
		}
		

		
		var json = {
			"class"   : [event,"org.ref_send.log.Event"],
			"anchor"  : {
				"number"  : number,
				"turn"    : {
					"loop"   : this.sourceURL,
					"number" : loopCount
			
				}
			},
			"message" :  id,
			"trace"   : {
				"calls": [
				{
					"name"   :   action,
					"source" : getClientId()
				}
				]
		
			}
		}
	
		this.messagesList.push(json);
		if(this.DEBUG_MODE) console.log('CAUSEWAY: '+JSON.stringify(this.messagesList));
	
	}
	
	
	this.getTrace = function(){
		return this.messagesList;
	}
	
	this.send = function(message,mos,channel){
		var params = {'idc':uniqueId()};
		message.addMetaData(params);
		
		this.generateTrace(message.selector,params.idc,'sent',message.loopCount);
	
		mos.next().send(message,mos.getPending(),channel);
		
	}
	
	
	
	
	this.receive = function(message,mos,channel){
		
		var idc = message.metaData.idc ? message.metaData.idc : uniqueId();
		
		this.generateTrace(message.selector,idc,'got',message.loopCount);
	
		mos.next().receive(message,mos.getPending(),channel);
		
	}
	
	
	

}

/*
*
Debugger MO - Client side
*
*/
function Debugger(serverMode){
	
	if (serverMode && (typeof localStorage === "undefined") || (localStorage === null)) {
		var LocalStorage = require('node-localstorage').LocalStorage;
		localStorage = new LocalStorage('./scratch');
	}

	
	this.debugSocket = serverMode  ? require('socket.io-client').connect('http://127.0.0.1:8125') : io.connect('http://127.0.0.1:8125',{},true);
  	this.validStatus = new Array('pause','resume','proceed','resend');
	this.statusName = {'pause':'pause','resume':'resume','proceed':'proceed','resend':'resend'};
	this.notSuspend = ['connection','connecting','connect','disconnect','reconnect','reconnecting'];
	this.mailbox = new Array();
  
	var private = this;
	
	this.debugSocket.on('debug@pause',function(info){
		if(info.client !== getClientId()) return;
		private.setStatus(private.statusName.pause);
	});
	
	this.debugSocket.on('debug@proceed',function(info){
		if((info.client !== getClientId()) || (private.getStatus() !== private.statusName.pause))  return;
		private.setStatus(private.statusName.resume);
		for(var i = 0; i < private.mailbox.length; i++){
			var pendObj = private.mailbox[i];
			if(pendObj['act'] === 'send') pendObj['mos'].next().send(pendObj['msg'],pendObj['mos'].getPending(),pendObj['ch']);
			else pendObj['mos'].next().receive(pendObj['msg'],pendObj['mos'].getPending(),pendObj['ch']);
		}
		private.mailbox = new Array();
	});
	
	this.debugSocket.on('debug@next',function(info){
		if((info.client !== getClientId()) || !private.mailbox.length || (private.getStatus() !== private.statusName.pause))  return;
		var pendObj = private.mailbox.next();
		private.mailbox = private.mailbox.getPending();
		if(pendObj['act'] === 'send') pendObj['mos'].next().send(pendObj['msg'],pendObj['mos'].getPending(),pendObj['ch']);
		else pendObj['mos'].next().receive(pendObj['msg'],pendObj['mos'].getPending(),pendObj['ch']);
	
	});
	
	this.debugSocket.on('debug@resend',function(info){
		if((info.client !== getClientId()) || (private.getStatus() !== private.statusName.pause))  return;
		for(var i = 0; i < private.mailbox.length; i++){
			var msg = private.mailbox[i]['msg'];
			if(msg.metaData.idm === info.message){
				var pendObj = private.mailbox[i];
				if(pendObj['act'] === 'send') pendObj['mos'].next().send(msg,pendObj['mos'].getPending(),pendObj['ch']);
				else pendObj['mos'].next().receive(msg,pendObj['mos'].getPending(),pendObj['ch']);
				private.mailbox.splice(i, 1);
				break;
			}
		}
	});
    
   	
    this.setStatus = function(status){
    	var currentStatus = 'normal';
    	if(this.validStatus.indexOf(status) !== -1) currentStatus = status;
    	localStorage["currentStatusDebugger"] = currentStatus;
    }
    
    this.getStatus = function(){
    	return localStorage["currentStatusDebugger"] ? localStorage["currentStatusDebugger"] : 'normal';
    	
    }
    
    this.setLatStatus = function(status){
    	var currentStatus = 'normal';
    	if(this.validStatus.indexOf(status) !== -1) currentStatus = status;
    	localStorage["lastStatusDebugger"] = currentStatus;
    }
    
    this.getLastStatus = function(){
    	return localStorage["lastStatusDebugger"] ? localStorage["lastStatusDebugger"] : 'normal';
    }
    
    
    this.establishTurn = function(message,channel,initialIdm,server_mode,operation){
    	var prev = initialIdm; //first message
    	if(operation === 'send'){
    		if(server_mode){
    			var tmp = channel.getContext().getLastTurn();
    			prev = tmp ? tmp.metaData.prev : initialIdm; //server returns response, checking if the response comes from a message
    		}
			else channel.getContext().getLastTurn();
			
			message.metaData['prev'] = prev;
    	}
    	else if(operation === 'receive'){
    		if(!message.metaData['prev']){ 
				var prev = message.metaData['idm'];
				if(server_mode){
					if(message.metaData['sender'] === 'server'){
						var tmp = channel.getContext().getLastTurn();
						prev = (tmp ? tmp.metaData.prev : message.metaData['idm'])
					}
					else{
						prev = message.metaData['idm'];
						channel.getContext().getLastTurn();
					}
				}
				else{ //receive after receive in the client
					var tmp = channel.getContext().getLastTurn();
					prev = (tmp ? tmp.metaData.prev : message.metaData['idm'])
				}
				message.metaData['prev'] = prev; //receive after send
			}
			else channel.getContext().getLastTurn(); //distributed, message sent is alike than the message received
    	}
    
    }
    
	
	this.send = function(message,mos,channel){
		
		
		var validSelector = this.notSuspend.indexOf(message.selector) === -1;
		
		if(message.metaData.mstatus && (message.metaData.mstatus === 'p')){  //second time that the message is sent
			message.metaData['mstatus'] = 's';
		}
    	else{
    		var idm = uniqueId();
			this.establishTurn(message,channel,idm,serverMode,'send');
			message.addMetaData({'cid': getClientId(), 'mstatus':'s', 'idm':idm,'ts':Date.now(),'sender': getClientId()});
    	}
    	
    	/*console.log('*****************************************');
    	console.log(message.selector);
    	console.log(message.metaData.prev);
    	
    	*/
    	var paused = false;
    	
    	if(!message.metaData.mbox && (this.getStatus() === this.statusName.pause) && validSelector){
    		mos.unshift(this); //this same component must resend the message to the client manager
    		message.metaData['mstatus'] = 'p';
    		message.addMetaData({'mbox':true});
    		var pendObj = {'msg': message, 'mos': mos, 'act':'send','ch':channel};
    		this.mailbox.push(pendObj);
    		paused = true;
    	}
    	
    	
    	this.debugSocket.emit(message.serialize()[0],message.serialize()[1]);
    	if(!paused){
			mos.next().send(message,mos.getPending(),channel);
		}

	}
	
	
	
	
	this.receive = function(message,mos,channel){
	
		var validSelector = this.notSuspend.indexOf(message.selector) === -1;
	
		if(message.metaData.mstatus && (message.metaData.mstatus === 'p')){  //second time that the message is sent
			message.metaData['mstatus'] = 's';
		}
    	else{
    		
			
    		message.metaData['sender'] = message.metaData['cid'] ? message.metaData['cid'] : getClientId();
			message.metaData['cid'] = getClientId();
			message.metaData['mstatus'] = 's';
			message.metaData['ts'] = Date.now();
			message.metaData['idm'] = message.metaData['idm'] ? message.metaData['idm'] : uniqueId();
			
			this.establishTurn(message,channel,message.metaData['idm'],serverMode,'receive');
    	}
		
		
		/*console.log('*****************************************');
    	console.log(message.selector);
    	
    	console.log(message.metaData.prev);*/
		
		var paused = false;
    	
    	if(!message.metaData.mbox && (this.getStatus() === this.statusName.pause) && validSelector){
    		mos.unshift(this); //this same component must resent the message to the client manager
    		message.metaData['mstatus'] = 'p';
    		message.addMetaData({'mbox':true});
    		var pendObj = {'msg': message, 'mos': mos, 'act':'receive','ch':channel};
    		this.mailbox.push(pendObj);
    		paused = true;
    	}
		
    	this.debugSocket.emit(message.serialize()[0],message.serialize()[1]);
    	if(!paused){
			mos.next().receive(message,mos.getPending(),channel);
		}
    	
	}

}

/*
*
Message class representation
*
*/
function Message(selector,data,rec){
	this.selector = selector;
	this.data = data;
	this.metaData = {};
	
	this.metaDataSeparator = '@';
	this.keyValueSeparator = ':';
	this.allowedDistributedMetaData = ['string','number','boolean'];
	
	if(rec && selector) this.deserialize(selector);
}


Message.prototype.addMetaData = function(p){
	for(i in p){
	   this.metaData[i] = p[i];
	}
}

Message.prototype.getMetaData = function(key){
	return this.metaData.hasOwnProperty(key) ? this.metaData[key] : null;
}

Message.prototype.serialize = function(){
	var newArgs = [this.selector,this.data];
	for (var k in this.metaData){
		if (this.metaData.hasOwnProperty(k)) {
			var tParam = typeof this.metaData[k];
			if (this.allowedDistributedMetaData.indexOf(tParam) !== -1){
				newArgs[0] += this.metaDataSeparator+k+this.keyValueSeparator+this.metaData[k];
			}
		}
	}
	return newArgs;
}

Message.prototype.deserialize = function(selector){
	
	var parts = selector.split(this.metaDataSeparator);
	this.selector = parts[0];
	for(var i = 0; i < parts.length; i++){
		if(i === 0) continue;
		var keyValue = parts[i].split(this.keyValueSeparator);
		if(keyValue.length > 1){
			this.metaData[keyValue[0]] = keyValue[1];
		}
	}
}


/*
*
JAD Framework
*
*/

function MIAJ(){
	var activeChannel = new AssociativeArray();
	
	this.subscribe = function(channel){
		channel.setContext(this);
		activeChannel[channel.constructor.name] = {'ac':0, m:new Array()};
	}
	
	this.unsubscribe = function(channel){
		channel.setContext(null);
	}
	
	this.setChannelTurn = function(ch,msg){
		if(activeChannel[ch.constructor.name].ac === 1){
			activeChannel[ch.constructor.name].m.push(msg);
		}
		else{
			for(i in activeChannel){
				if(activeChannel.hasOwnProperty(i)) {
					activeChannel[i].ac = 0;
					activeChannel[i].m = new Array();
				}
			}
			activeChannel[ch.constructor.name].ac = 1;
			activeChannel[ch.constructor.name].m = [msg];
		}
		
	}
	
	this.getLastTurn = function(){
		for(i in activeChannel){
			if(activeChannel.hasOwnProperty(i) && activeChannel[i].ac === 1){
				var res = activeChannel[i].m.next();
				activeChannel[i].m = activeChannel[i].m.getPending();
				return res;
			}
		}
		return null;
	}
}


function Default(){
	this.send = function(msg,mos,channel){
		if(channel.getContext()){
			channel.getContext().setChannelTurn(channel,msg);
			channel.send(msg);
		}
	}
	
	this.receive = function(msg,mos,channel){
		if(channel.getContext()){
			channel.getContext().setChannelTurn(channel,msg);
			channel.receive(msg);
		}
	}
}

function SocketIOChannel(socket){

	this.socket = socket
	this.emitOr = socket.emit;
	this.onOr = socket.$emit;
	
	this.context = null;
	
	var private = this;
	var MOS = [];
	
	this.setMetachannels = function(mos){
		MOS = mos;
	}
	
	this.setContext = function(c){
		this.context = c;
	}
	this.getContext = function(){
		return this.context;
	};
	
	this.send = function(msg){
		this.emitOr.apply(this.socket,msg.serialize());
	};
	
	this.receive = function(msg){
		this.onOr.apply(this.socket,[msg.selector,msg.data]);
	}
	socket.emit = function(){
		var msg = new Message(arguments[0],arguments[1]);
		
		
		msg.addMetaData({'event':'emit'});
		MOS.next().send(msg,MOS.getPending(),private);
	};
	
	socket.$emit = function(){
		if(!MOS.length) return;
		var msg = new Message(arguments[0],arguments[1],true);
		msg.addMetaData({'event':'on'});
		MOS.next().receive(msg,MOS.getPending(),private);	
	}
	
	
	
	
	
	this.emit = function(selector,data){
		socket.emit(selector,data);
	}
	this.on = function(selector,data){
		socket.on(selector,data);
	}
	
	
	
	
	
}




function MySQLChannel(connection){

	this.connectionQuery = connection.query;
	this.context = null;
	
	var MOS = [];
	var private = this;
	
	connection.query = function(){
		
		var idq = uniqueId();
		
		var message = new Message('query '+idq,arguments[0]);
		message.addMetaData({'event':'emit','idq': idq,'args':arguments,'ref':this});
	
		MOS.next().send(message,MOS.getPending(),private);	
		
	}
	
	this.on = function(message,args,ref,lambda){
		//message.setArgs(['queryResponse '+message.metaData.idq,args[1]]);
		message.selector = 'queryResponse '+message.metaData.idq;
		message.data = args[1];
		message.addMetaData({'event':'on','lambda':lambda, 'args':args,'ref':ref});
		MOS.next().receive(message,MOS.getPending(),private);
	}
	
	this.setMetachannels = function(mos){
		MOS = mos;
	}
	
	this.setContext = function(c){
		this.context = c;
	}
	this.getContext = function(){
		return this.context;
	};
	
	this.send = function(message,mos){
		if(typeof message.metaData.args[1] === 'function'){ //changing lambda for wrapit
			var lambda = eval(message.metaData.args[1]);
			message.metaData.args[1] = function(){
				private.on(message,arguments,this,lambda);
			}
		}
		this.connectionQuery.apply(message.metaData.ref,message.metaData.args);
	};
	
	this.receive = function(msg,mos){
		msg.metaData.lambda.apply(msg.metaData.ref,msg.metaData.args);
	}
	
}



function jQueryChannel(){
	var methodsToWrap = [{method:'html',type:'receive',cb:false},{method:'click',type:'receive',cb:true}];
	var captureMouse = true; //set true if there is some mouse event in the list above
	this.callbackList = [];
	this.orWrappers = [];
	this.context = null;
	
	var MOS = new Array();
	
	var private = this;
	
	var initMethod = function(info){
		private.orWrappers[info.method] = jQuery.fn[info.method];
		if(!info.cb){ //is not a callback event
			jQuery.fn[info.method] = function(){//info.method = html
				if(!arguments.length) return private.orWrappers[info.method].apply(this,arguments);
				var message = new Message(info.method,arguments[0]);
				var event = info.type === 'send' ? 'domout' : 'domin';
				message.addMetaData({'event':event,'ref':this, 'args':arguments, 't':'mu'});

				switch(info.type){//info.type = receive
					case 'send': MOS.next().send(message,MOS.getPending(),private); break;
					case 'receive': MOS.next().receive(message,MOS.getPending(),private); break;
					default: break;
				}
			}
		}
		else{		
					
			jQuery.fn[info.method] = function(){ 
				if(!this[0].localId) this[0].localId = uniqueId();
				if(!private.callbackList[this[0].localId]) private.callbackList[this[0].localId] = new Array();
				private.callbackList[this[0].localId].push({'ev':info.method, 'cb':arguments[0]});
			}
		}
	}
	for(var i = 0; i < methodsToWrap.length; i++){
		initMethod(methodsToWrap[i]);
	}
	var doActionWithEvent = function(e,type){

  		e = e ||  window.event;
		var element = e.target || e.srcElement;
		var isLink = e.srcElement.getAttribute('href');
		var target = e.srcElement.getAttribute('target');
		
		var indexCallback = -1;
		
		if(element.localId){
			var callbacks  = private.callbackList[element.localId];
			for(ind in callbacks){
				if(callbacks[ind].ev === type){ indexCallback = ind; break; }
			}
			
		}
		if(indexCallback !== -1){ //element with listener
			var idm = uniqueId();
			var message = new Message(type,private.callbackList[element.localId][indexCallback].cb.toString());
			message.addMetaData({'event':'domin','idcb':indexCallback,'lid':element.localId, 't':'cb' });
			MOS.next().receive(message,MOS.getPending(),private);        	
		}
		else if(isLink){ //A element
			e.preventDefault();
			var message = new Message(type,isLink);
			message.addMetaData({'event':'domin','idcb':-1, 'lid':null, 't':'li', 'tg':target });
			MOS.next().receive(message,MOS.getPending(),private);  
		
		}
		else{ //one of the antecessors of the element is A
			e.preventDefault();
			var finish = false;
			var pathElem = e.srcElement.parentNode;
			
			var i = 0;
			var isLink = null;
			while(!finish){
				isLink = pathElem.tagName === 'A' ? pathElem.getAttribute('href') : '';
				pathElem = pathElem.parentNode;
				finish = isLink || !pathElem;	
			}
			if(isLink){
				var idm = uniqueId();
				var message = new Message(type,isLink,true);
				message.addMetaData({'event':'domin','idcb':-1, 'lid':null, 't':'li', 'tg':target });
				MOS.next().receive(message,MOS.getPending(),private);  
			}
		}
  	}
	
	
	if(captureMouse){
		document.onclick = function(e){
			doActionWithEvent(e,'click');
		}
	}
	
	this.setMetachannels = function(mos){
		MOS = mos;
	}
	
	this.setContext = function(c){
		this.context = c;
	}	
	this.getContext = function(){
		return this.context;
	};
	
	this.send = function(message){
		return this.orWrappers[message.selector].apply(message.metaData.ref, message.metaData.args);
	}
	
	this.receive = function(message){
		switch(message.metaData.t){
			case 'mu': 
				this.orWrappers[message.selector].apply(message.metaData.ref, message.metaData.args); 
				break;
			case 'li': 
				window.open(message.data,message.metaData.tg ? message.metaData.tg : '_self');
				break;
			case 'cb':
				var callback = eval(this.callbackList[message.metaData.lid][message.metaData.idcb].cb);
				callback();
				break;
			default: break;
		}
	}
	
	
}


