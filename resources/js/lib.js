	function createItemListImage(item){
		return '<div class="col-xs-6 col-sm-3">'
              +'<a href="/book_detail?id='+item.id+'"><img src="resources/img/'+item.image+'" class="img-thumbnail img-responsive home_img" alt="'+item.image+'"></a>'
              +'<h4>'+item.title+'</h4>'
              +'<span class="text-muted">'+item.description+'</span>'
            +'</div>';
	
	}
	
	function createItemListTable(item){
		return '<tr>'
                  +'<td>'+item.amount+'</td>'
                  +'<td><a href="/book_detail?id='+item.id+'">'+item.title+'</a></td>'
                  +'<td>'+item.description+'</td>'
                  +'<td>'+item.isbn+'</td>'
                  +'<td>'+(item.amount-item.pend_reqs)+'</td>'
                +'</tr>';
	
	}
	
	function createItemListRequests(item){
		return '<tr>'
                  +'<td><a href="/book_detail?id='+item.id+'">'+item.title+'</a></td>'
                  //+'<td>'+item.description+'</td>'
                  +'<td>'+item.isbn+'</td>'
                  +'<td>'+((item.stat) == 'P' ? '<span class="label label-warning">Pending</span>' : '<span class="label label-success">Returned</span>')+'</td>'
                  +'<td>'+item.date_lend+'</td>'
                  +'<td>'+((item.date_to_return == '0000-00-00 00:00:00') ? 'No' : item.date_to_return)+'</td>'
                +'</tr>';
	
	}
	
	function createBookInfo(item){
	
		return '<label>Title:</label> <span id="title">'+item.title+'</span><br/>'
        	+'<label>Code:</label> <span id="code">'+item.code+'</span><br/>'
        	+'<label>ISBN:</label> <span id="isbn">'+item.isbn+'</span><br/>'
        	+'<label>Description:</label> <span id="description">'+item.description+'</span><br/>'
        	+'<label>Authors:</label> <span id="authors">'+item.authors+'</span><br/>'
        	+'<label>Amount:</label> <span id="amount">'+item.amount+'</span><br/>'
        	+'<label>Keywords:</label> <span id="keywords">'+item.keywords+'</span><br/>';
	}