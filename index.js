//requirements modules
var express=require('express');
var swig  = require('swig');
var url  = require('url');
var fs = require('fs');
var qs = require('querystring');
var mysql = require('mysql');
var http = require('http');
var thisUrl = __dirname+'/index.js';
var Q = require("q");
var qConnection = require("q-connection");


//templates
var index_tpl = swig.compileFile(__dirname+'/templates/index.html');
var requests_tpl = swig.compileFile(__dirname+'/templates/requests.html');
var history_tpl = swig.compileFile(__dirname+'/templates/history.html');
var addbook_tpl = swig.compileFile(__dirname+'/templates/addbook.html');
var book_detail_tpl = swig.compileFile(__dirname+'/templates/bookdetail.html');

var indexOutput = index_tpl({pageName:'index'});
var requestsOutput = requests_tpl({pageName:'requests'});
var historyOutput = history_tpl({pageName:'history'});
var addbookOutput = addbook_tpl({pageName:'addBook'});
var bookDetailOutput = book_detail_tpl({id_book:-1});
var connection = mysql.createConnection({
  		host     : 'localhost',
  		user     : 'root',
  		password : '',
  		datbase  : 'library'
});
connection.connect(function(err) {
  		if(err) throw err;
});	




//app
var app = http.createServer(handlerIndex);


//creating the socket
io = require('socket.io').listen(app,{ log: false });

//starting to listen
app.listen(8124, "127.0.0.1");

var path = '/';

function handlerIndex(req,res){
	
	//getting get and post data
	var GET = {};
	var POST = {};
	
	var url_parts = url.parse(req.url, true);
	GET = url_parts.query;
    
	if (req.method == 'GET') {
        var body = '';
        req.on('data', function (data) {
            body += data;
            if (body.length > 1e6) { 
                // FLOOD ATTACK OR FAULTY CLIENT, NUKE REQUEST
                req.connection.destroy();
            }
        });
        req.on('end', function () {
            POST = qs.parse(body);
        });
    }
    
    //app controller, handles all accesses to the app
	path = url.parse(req.url).pathname;
    switch (path){
        case '/':
            res.writeHead(200,{'Content-Type' : 'text/html'});
  			res.write(indexOutput);
  			res.end();
            break;
        case '/requests':
        	res.writeHead(200,{'Content-Type' : 'text/html'});
  			res.write(requestsOutput);
  			res.end();
            break;
        case '/history':
        	res.writeHead(200,{'Content-Type' : 'text/html'});
  			res.write(historyOutput);
  			res.end();
            break;
            
        case '/addbook':
			res.writeHead(200,{'Content-Type':'text/html'});
  			res.write(addbookOutput);
  			res.end();
            break;
        case '/book_detail':
			res.writeHead(200,{'Content-Type':'text/html'});
			bookDetailOutput = book_detail_tpl({id_book:GET.id});
  			res.write(bookDetailOutput);
  			res.end();
            break;
        case '/ajax':
			res.writeHead(200,{'Content-Type':'text/plain'});
  			res.write("Ajax response");
  			res.end();
            break;
        default:
            if (/\.(css)$/.test(path)){
                res.writeHead(200, {'Content-Type': 'text/css'});
        		res.write(fs.readFileSync(__dirname + path,  'utf8'),'utf8');
        		res.end();
            }
            else if (/\.(js)$/.test(path)){
                res.writeHead(200, {'Content-Type': ' application/x-javascript'});
        		res.write(fs.readFileSync(__dirname + path,  'utf8'),'utf8');
        		res.end();
            }
            else if (/\.(jpg|png|gif)$/.test(path)){
                res.writeHead(200, {'Content-Type': ' image/png'});
        		try{
        			var tmp = fs.readFileSync(__dirname + path,  'binary');
        			res.write(tmp,'binary');
        		}
        		catch(err){}
        		
        		res.end();
            }
			else{
            	res.writeHead(200, {'Content-Type': 'text/plain'});
    			res.write(res);
    			res.end();
    		}
            
            break;
    }
}



function updateIndexListBySocket(socket,data,broadcast){
	//var Response = {lendList:new Array(), bookList:new Array()};
    connection.query("SELECT b.*, COUNT(r.id) AS pend_reqs FROM library.book b LEFT JOIN library.book_request r ON b.id = r.id_book AND r.stat = 'P' GROUP BY b.id order by b.created DESC", function(err, booksl, fields) {	
    	if(err) booksl = new Array();
    	if(broadcast) socket.broadcast.emit('indexListResponse', booksl);
    	else socket.emit('getBooksToListResponse', booksl);
	});
	
}

function updateIndexImageBySocket(socket,data,broadcast){
	connection.query('SELECT * FROM library.book where deleted = 0 order by created desc limit 0,4', function(err, booksi, fields) {
    	if(err) booksi = new Array();
    	if(broadcast) socket.broadcast.emit('indexImageResponse', booksi);
    	else socket.emit('getBooksToImageResponse', booksi);
   	});

}


  eval(fs.readFileSync(__dirname+"/resources/js/tracer.js")+'');
  var miaj = new MIAJ();
  
 var meta_channels = [new CAUSEWAY(false),new Debugger(true),new Default()];
	
  
  var mysql = new MySQLChannel(connection);
  mysql.setMetachannels(meta_channels);
  miaj.subscribe(mysql);
  
io.sockets.on('connection', function (socket) {
		
 socket = new SocketIOChannel(socket);
 socket.setMetachannels(meta_channels);
 miaj.subscribe(socket);

  
  
  
  socket.on('getBooksToList', function (data) {  		
  		updateIndexListBySocket(socket,data,false); //sending message only to sender
  });
  
  socket.on('getBooksToImage', function (data) {
  		//updateIndexListBySocket(socket,data,false); //sending message only to sender
  		updateIndexImageBySocket(socket,data,false); //sending message only to sender
  		
  });
  
  socket.on('getBooksToImages', function (data) {
  		updateIndexImageBySocket(socket,data,false); //sending message only to sender
  		
  });
  
  socket.on('requestsReady', function (data) {
		
    	connection.query("SELECT b.*,r.stat, date_format(r.date_lend,'%d-%m-%Y %H:%i') as date_lend,date_format(DATE_ADD(r.date_lend, INTERVAL 7 DAY),'%d-%m-%Y %H:%i') as date_to_return FROM library.book_request r INNER JOIN library.book b ON r.id_book = b.id WHERE r.id_user = 1 and r.stat = 'P' ORDER BY r.stat, r.date_lend", function(err, requests, fields) {
    		
    		if(err) requests = new Array();
    
    		socket.emit('requestsResponse', requests);
    	});
  });
  
  socket.on('historyReady', function (data) {
		
    	connection.query("SELECT b.*,r.stat, date_format(r.date_lend,'%d-%m-%Y %H:%i') as date_lend,date_format(DATE_ADD(r.date_lend, INTERVAL 7 DAY),'%d-%m-%Y %H:%i') as date_to_return FROM library.book_request r INNER JOIN library.book b ON r.id_book = b.id WHERE r.id_user = 1 and r.stat = 'R' ORDER BY r.stat, r.date_lend", function(err, requests, fields) {
    		
    		if(err) requests = new Array();
    
    		socket.emit('historyResponse', requests);
    	});
  });
  
  socket.on('newBook', function (data) {
  
		var query = "INSERT INTO library.book(title,code,isbn,description,authors,amount,image,keywords) VALUES('"+data.title+"','"+data.code+"','"+data.isbn+"','"+data.description+"','"+data.authors+"','"+data.amount+"','"+data.image+"','"+data.keywords+"')";
		connection.query(query, function(err, result) {
    		updateIndexListBySocket(socket,data,true); //sending message to everyone but sender
    		socket.emit('newbookResponse', {});
		});
    	
    	
    	
  });
  
  socket.on('book_info',function(data){
  		
  		var query = "SELECT * from library.book where id = "+data.id_book;
		connection.query(query, function(err, result) {
			if(err) result = [];
    		socket.emit('book_info_response', result);
    		
		});
  
  
  
  });
  

  
  socket.on('book_available',function(data){
  	var result = {status:  data.id_book > 0 ? true : false};
  	var sleep = require('sleep');
  	sleep.sleep(3);
  	socket.emit('book_available_response', result);
  });
  
  socket.on('user_can_lend',function(data){
  	var result = {status: data.id_book > 0 ? true : false};
  	socket.emit('user_can_lend_response', result);
  });
  
  socket.on('user_blocked',function(data){
  	var result = {status: data.id_book > 0 ? false : true};
  	socket.emit('user_blocked_response', result);
  });
  
  
  
});








console.log('Server running at http://127.0.0.1:8124/');
