//requirements modules
var express=require('express');
var swig  = require('swig');
var url  = require('url');
var qs = require('querystring');
var fs = require('fs');
var http = require('http');
var thisUrl = __dirname+'/index.js';
var path = require('path');

//templates
var index_tpl = swig.compileFile(__dirname+'/templates/index.html');
var detail_tpl = swig.compileFile(__dirname+'/templates/detail.html');

var indexOutput = index_tpl({pageName:'index'});
var detailOutput = detail_tpl({pageName:'detail',idc:-1});


//app
var app = http.createServer(handlerIndex);

var messages = new Array();




io = require('socket.io').listen(app,{ log: false });


io.sockets.on('connection', function (socket) {
	
	
	var emit = socket.emit;
	var on = socket.$emit;
	
	
	
	socket.$emit = function(){
		
		
		//console.log("okkk");
		console.log("------------------->"+JSON.stringify(arguments));
		
    	
		var parts = arguments[0].split("@");
		if((parts.length > 1) && parts[0] === 'debug'){
			socket.broadcast.emit(arguments[0], arguments[1]);
		}
		
		else if((parts.length > 1) && parts[0] === 'debugClient'){
			arguments[0].replace('debugClient','debug');
			socket.emit(arguments[0], arguments[1]);
		}
		
		else {
			
			socket.broadcast.emit('debugger_result_client', arguments);
		}
	
	
	}
	

	
});







//while(webSocket == null){}

var path = '/';

function handlerIndex(req,res){
	
	
	
	//getting get and post data
	var GET = {};
	var POST = {};
	
	var url_parts = url.parse(req.url, true);
	GET = url_parts.query;
    
	if (req.method == 'GET') {
        var body = '';
        req.on('data', function (data) {
            body += data;
            if (body.length > 1e6) { 
                // FLOOD ATTACK OR FAULTY CLIENT, NUKE REQUEST
                req.connection.destroy();
            }
        });
        req.on('end', function () {
            POST = qs.parse(body);
        });
    }
    
    //app controller, handles all accesses to the app
	path = url.parse(req.url).pathname;
    switch (path){
        case '/':
            res.writeHead(200,{'Content-Type' : 'text/html'});
            
  			res.write(indexOutput);
  			res.end();
            break;
            
        case '/detail':
            res.writeHead(200,{'Content-Type' : 'text/html'});
            detailOutput = detail_tpl({pageName:'detail',idc:GET.idc});
  			res.write(detailOutput);
  			res.end();
            break;

        default:
            if (/\.(css)$/.test(path)){
                res.writeHead(200, {'Content-Type': 'text/css'});
        		res.write(fs.readFileSync(__dirname + path,  'utf8'),'utf8');
        		res.end();
            }
            else if (/\.(js)$/.test(path)){
                res.writeHead(200, {'Content-Type': ' application/x-javascript'});
        		res.write(fs.readFileSync(__dirname + path,  'utf8'),'utf8');
        		res.end();
            }
            else if (/\.(jpg|png|gif)$/.test(path)){
                res.writeHead(200, {'Content-Type': ' image/png'});
        		try{
        			var tmp = fs.readFileSync(__dirname + path,  'binary');
        			res.write(tmp,'binary');
        		}
        		catch(err){}
        		
        		res.end();
            }
            else if (/\.(woff)$/.test(path)){
                res.writeHead(200, {'Content-Type': 'application/x-font-woff'});
        		try{
        			var tmp = fs.readFileSync(__dirname + path,  'binary');
        			res.write(tmp,'binary');
        		}
        		catch(err){}
        		
        		res.end();
            }
            else if (/\.(ttf)$/.test(path)){
                res.writeHead(200, {'Content-Type': 'application/x-font-ttf'});
        		try{
        			var tmp = fs.readFileSync(__dirname + path,  'binary');
        			res.write(tmp,'binary');
        		}
        		catch(err){}
        		
        		res.end();
            }
			else{
            	res.writeHead(200, {'Content-Type': 'text/plain'});
    			res.write(res);
    			res.end();
    		}
            
            break;
    }
}

app.listen(8125, "127.0.0.1");

console.log('Debugger at http://127.0.0.1:8125/');
