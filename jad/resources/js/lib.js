	function createItemListImage(item){
		return '<div class="col-xs-6 col-sm-3">'
              +'<a href="/book_detail?id='+item.id+'"><img src="resources/img/'+item.image+'" class="img-thumbnail img-responsive home_img" alt="'+item.image+'"></a>'
              +'<h4>'+item.title+'</h4>'
              +'<span class="text-muted">'+item.description+'</span>'
            +'</div>';
	
	}
	
	function createItemListTable(item){
		return '<tr>'
                  +'<td>'+item.amount+'</td>'
                  +'<td><a href="/book_detail?id='+item.id+'">'+item.title+'</a></td>'
                  +'<td>'+item.description+'</td>'
                  +'<td>'+item.isbn+'</td>'
                  +'<td>'+(item.amount-item.pend_reqs)+'</td>'
                +'</tr>';
	
	}
	
	function createItemListRequests(item){
		return '<tr>'
                  +'<td><a href="/book_detail?id='+item.id+'">'+item.title+'</a></td>'
                  //+'<td>'+item.description+'</td>'
                  +'<td>'+item.isbn+'</td>'
                  +'<td>'+((item.stat) == 'P' ? '<span class="label label-warning">Pending</span>' : '<span class="label label-success">Returned</span>')+'</td>'
                  +'<td>'+item.date_lend+'</td>'
                  +'<td>'+((item.date_to_return == '0000-00-00 00:00:00') ? 'No' : item.date_to_return)+'</td>'
                +'</tr>';
	
	}
	
	Storage.prototype.setObject = function(key, value) {
    	this.setItem(key, JSON.stringify(value));
	}

	Storage.prototype.getObject = function(key) {
    	return JSON.parse(this.getItem(key));
	}
  	
  	function getDate(){
  		var date = new Date();
    	return date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

  	}
	
	function Params(){
	
	}
	
	function demarshal(args){
		
		
		var parts = args[0].split('@');
		var params = new Params();
		args[0] = parts[0];

		if(parts.length == 1 || !(parts instanceof Array)) return params;
		for(var i = 0; i < parts.length; i++){
			if(i === 0) continue;
			var keyValue = parts[i].split(':');
			if(keyValue.length > 1){
				params[keyValue[0]] = keyValue[1];
			}
		}
		return params;
	}
	
	 $(document).keypress("c",function(e) {
  		if(e.ctrlKey) {localStorage.clear(); alert('LocalStorage cleaned.')}
	});